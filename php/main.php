<?php

/* Main RunPHP BOOTSTRAP
 * @since 2013.09.27
 *
 * TODO:
 *
 */

//Defines for CORE
defined('PHP') || define('PHP', __DIR__ . '/');
defined('LIB') || define('LIB', PHP . 'lib/');
defined('MODEL') || define('MODEL', PHP . 'models/');
defined('CONTROLLER') || define('CONTROLLER', PHP . 'controller/');
defined('VIEW') || define('VIEW', PHP . 'view/');
defined('SUBURI') || define('SUBURI', '');

//Defines from template
defined('PATH') || define('PATH', dirname($_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_NAME']) . '/');
defined('RPATH') || define('RPATH', ((strpos(PATH, 'phar://') === false) ? PATH : str_replace('phar://', '', dirname(PATH) . '/')));
defined('HTML') || define('HTML', PATH . 'html/');
defined('HTMLEXT') || define('HTMLEXT', '.html');
defined('STYLE') || define('STYLE', PATH . 'style/');
defined('SCRIPT') || define('SCRIPT', PATH . 'script/');
defined('IMAGE') || define('IMAGE', PATH . 'img/');

//capturing output
ob_start();

//starting the autoloader classes (Autoloader)
set_include_path('.' . PATH_SEPARATOR . str_replace('phar:', 'phar|', LIB)
        . PATH_SEPARATOR . str_replace('phar:', 'phar|', PHP)
        . trim(get_include_path(), ' .'));

//setting the automatic loading - Autoloader
spl_autoload_register(
        function ($class) {
            $class = ltrim('/' . strtolower(trim(strtr($class, '_\\', '//'), '/ ')), ' /\\') . '.php';
            $pth = explode(PATH_SEPARATOR, ltrim(get_include_path(), '.'));
            array_shift($pth);
            foreach ($pth as $f) {
                if (file_exists($f = str_replace('phar|', 'phar:', $f) . $class))
                    return require_once $f;
            }
        }
);
//include the autoloader Composer, if any.
if (file_exists(LIB . 'autoload.php')) include LIB . 'autoload.php';

//URL_BASE = base para os arquivos html => <base href="<?php echo URL_BASE;? >" />
if (!isset($_SERVER['REQUEST_SCHEME'])) $_SERVER['REQUEST_SCHEME'] = 'http';
define('URL_BASE', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/' . SUBURI);

//runing application controller -> action
define('URL', urldecode(trim(str_replace(SUBURI, '', substr($_SERVER['REQUEST_URI'], strlen('/'))), ' /') . '/'));

//alias class
class_alias('Lib\Book\Config\Config', '_config');

//loading config file
_config::load(PHP . 'config.json');

//modeling the application output
output(frontController(URL));

//finish -------------------------------------------------------


/* Front Controller
 * Running the application front controller
 *
 */
function frontController($url = '', $defCtrl = 'main', $defAction = 'index', $Epage = '404.html') {
    //breaking the url . . .
    $url = explode('/', $url);

    //finding a controller -------------------------------------
    $controller = '/'.strtolower((isset($url[0]) && $url[0] != '') ? $url[0] : ''); //default
    
    //special no controllers
    $cfg = _config::get(); 
    foreach($cfg->Routers as $k=>$v){
        if(trim($controller, '/') == $k) {
            array_shift($url);
            $v($url);
        }
    }
    
    //get the ROUTE in DB
    $hdl = Lib\Book\Db\Db::hdl();
    $sth = $hdl->prepare('SELECT SOLVE FROM 300k.BOOK WHERE ROUTE = :route');
    $sth->execute(array(':route'=>$controller));
    
    $res = $sth->fetch(PDO::FETCH_ASSOC);
    $controller = strtolower((!$res) ? $defCtrl : $res['SOLVE']);
    
    //passing control to the controller class
    $pathCtrl = CONTROLLER . $controller . '.php';
    if (file_exists($pathCtrl)) include $pathCtrl;
    elseif (file_exists(HTML . '404.php')) {
        include HTML . '404.php';
        exit();
    } elseif (file_exists(CONTROLLER . $defCtrl . '.php')) {
        include CONTROLLER . $defCtrl . '.php';
        $controller = $defCtrl;
        array_unshift($url, $defCtrl);
    } else exit('Controller "' . ucfirst($controller) . '" não encontrado!');
    
    //new controller
    $controller = ucfirst($controller);
    $controller = new $controller;

    //finding a action -----------------------------------------
    $action = (isset($url[1]) && $url[1] != '' && method_exists($controller, $url[1])) ? strtolower($url[1]) : $defAction; //default
    if ($action != $defAction) array_shift($url);

    //collecting parameters ------------------------------------
    array_shift($url);
    if (!is_array($url)) $url = Array();

    //run controller action and return -------------------------
    return call_user_func_array(array($controller, $action), $url);
}

/* OUT
 * Modeling the application output
 *
 */

function output($app = '') {
    ob_end_clean();
    ob_start('ob_gzhandler');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 31536000) . ' GMT');
    header('Cache-Control: must_revalidate, public, max-age=31536000');
    exit($app);
}

/* RUNSTYLE
 * Modeling the assets output
 * Link format: <link href="runstyle/main|reset"  rel="stylesheet" type="text/css"/>
 *
 */
function runstyle($url){
    $url = implode('/', $url);    
    $cache = STYLE.'cache/'.md5(str_replace(array('|','/'), '_', $url)).'.cache';
    
    //create cache file (if not exists)
    if(!file_exists($cache)) {
        $files = explode('|', '|'.trim($url, ' /'));
        $dt = '';
        foreach ($files as $file) {
            if(file_exists(STYLE.$file.'.css'))
                $dt .= preg_replace("#/\*[^*]*\*+(?:[^/*][^*]*\*+)*/#","",
                       preg_replace('<\s*([@{}:;,]|\)\s|\s\()\s*>S','\1',
                       str_replace(array("\n","\r","\t"),'',
                       file_get_contents(STYLE.$file.'.css'))))."\n\n";
        }    
        file_put_contents($cache, $dt);
    }
    download('css', $cache);
}

/* RUNSCRIPT
 * Modeling the assets output
 * Link format: <script scr="runscript/main|lib/jquery/jquery-ui"></script>
 *
 */
function runscript($url){
    $url = implode('/', $url);    
    $cache = SCRIPT.'cache/'.md5(str_replace(array('|','/'), '_', $url)).'.cache';    
    
    //create cache file (if not exists)
    if(!file_exists($cache)) {
        $files = explode('|', '|'.trim($url, ' /'));
        $dt = ''; //p($files, true);
        foreach ($files as $file) {
            if(file_exists(SCRIPT.$file.'.js'))
                $dt .= preg_replace("/^\s/m",'',
                       str_replace("\t",'',
                       file_get_contents(SCRIPT.$file.'.js')))."\n\n";
        }
        file_put_contents($cache, $dt);
    }
    download('js', $cache);
}

/* DOWNLOAD
 * 
 *
 */
function download($ext, $path){
    //procurando o mime type
    include LIB.'neos/mimes.php';
    if (!isset($_mimes[$ext])) $mime = 'text/plain';
    else $mime = (is_array($_mimes[$ext])) ? $_mimes[$ext][0] : $_mimes[$ext];
    
    //pegando o arquivo
    $dt = file_get_contents($path);
    
    //enviando
    ob_end_clean();
    ob_start('ob_gzhandler');

    header('Vary: Accept-Language, Accept-Encoding');
    header('Content-Type: ' . $mime);
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 31536000) . ' GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
    header('Cache-Control: must_revalidate, public, max-age=31536000');
    header('Content-Length: ' . strlen($dt));
    header('x-Server: nfw/RunPHP');
    header('ETAG: '.md5($path));
    //saindo...
    exit($dt);
}

/* PRINT
 * Simple p() function
 */

function p($val, $exit = false) {
    if($exit) exit('<pre>' . print_r($val, true) . '</pre>');
    echo '<pre>' . print_r($val, true) . '</pre>';
}
