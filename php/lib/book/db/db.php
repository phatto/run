<?php

/**
 * Description of connect
 *
 * @author Paulo
 */

namespace Lib\Book\DB;
use Lib\Book\Config\Config;
use PDO;

class Db {

    static $conn = Array();
    static $resultClass = 'Lib\Book\DB\Result';

    final public static function hdl($alias = null) {
        $cfg = Config::get('DB');
        if($alias == null) $alias = $cfg->default;
        if (!isset($cfg->$alias))
            trigger_error('Alias "' . $alias . '" DB not configured', E_USER_ERROR);
        $cfg = $cfg->$alias;

        //check if handler exists
        if (isset(static::$conn[$alias]) && is_object(static::$conn[$alias]))
            return static::$conn[$alias];
        return static::$conn[$alias] = new PDO($cfg->driver . ':host=' . $cfg->host, $cfg->user, $cfg->password);
    }
    
    static function query($sql, $bind = Array(), $type = null, $alias = null){
        $cfg = Config::get('DB');
        if($alias == null) $alias = $cfg->default;
        if(!isset(static::$conn[$alias])) $hdl = self::hdl($alias);
        $hdl = static::$conn[$alias];
        
        //result class type
        if($type == null) $type = self::$resultClass;
        
        //PDO
        $sth = $hdl->prepare($sql);
        $sth->execute($bind);
        while ($row = $sth->fetchObject ($type, array('SQL'=>$sql, 'Bind'=>$bind))) {
            $data[] = $row;
        }
        return $data;
        return $sth->fetchObject ($type, array('SQL'=>$sql, 'Bind'=>$bind));        
    }

}
