<?php

/**
 * Description of controller
 *
 * @author Paulo
 */

namespace Lib\Neos;

use Lib\Neos\Base;
use Lib\Neos\Phen as View;

class Controller extends Base {

  public function __construct() {
    $this->View = new View();
  }

}

?>
