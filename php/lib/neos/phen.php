<?php

/**
 * Description of phen
 *
 * @author Paulo
 */

namespace Lib\Neos;

use Lib\Neos\Base;

class Phen extends Base {

  private $view = 'defalt';
  private $viewpath = PVIEW;
  private $scriptpath = SCRIPT;
  private $stylepath = STYLE;
  private $imagepath = IMAGE;

  private $views = Array();
  private $values = Array();

  static function __callStatic($name, $arguments) {
    $name = str_replace('set', '', strtolower($name));
    return ((isset(self::this()->$name)) ? self::this()->$name = $arguments[0] : false);
  }

  static function val($val = 'nada') {
    echo '<p>function val: ' . $val . '</p>';
  }

  static function set($val = 'nada') {
    echo '<p>function set: ' . $val . '</p>';
  }

  // ----------------- no Static functions ------------------------------------
  function __call($name, $arguments) {
    $name = str_replace('set', '', strtolower($name));
    return ((isset($this->$name)) ? $this->$name = $arguments[0] : false);
  }

  function assign($var = 'nada', $value = '') {
    $this->values[$var] = $value;
    echo '<p>function value: ' . $var . '</p>';
  }
  function load($val = 'nada', $name = 'head') {
    $this->$name = new Phen();
    $this->$name->setView($val);
    return $this->$name;

    $this->views[] = $val;
    echo '<p>function insert: ' . $val . '</p>';
  }



  function render(){
    echo '<pre>'.print_r($this, true).'</pre>';

  }

}

?>
