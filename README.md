This project has been aborted - see http://github.com/pedra to find similar production.
==
***

RUN
===
***Mini starter for PHP site application***

*Att.: This is not a finished product. Is in development and will probably not work satisfactorily.*


 * Supporting MVC
 * Ability to run on a PHAR mode
 * Autorun fully compatible with PSR-0 standard
 * Partial implemetation (98%) PSR-1,2 & 3 
 * Pure PHP without learning anything more

Contacts
--------
***Contribua com esse projeto***

 * E-mail:	prbr@ymail.com
 * Skype:	neosphp
 * Tel.:	(+55 21) 3630-5920
 * Site:	http://colabore.co.vu/run

Start
-----
1. Copy the contents of this repository to a folder on your web server root (or Git clone);
2. Turn "http://localhost/{path}/" in your web browser.



***Another possibility is to use the Composer:***

1. Create a folder in the root directory of your web server;
2. Create and put the following content in the file "composer.json":

		{
    		"require": {
    			"php": ">=5.3.0",
       			"neos/run": "*.2"
			}
		}
		
3. In the terminal, type: "composer install"

----------
***Excuse me! Part of this software is in Portuguese!***

***Please help me to translate into English.***

----------